﻿using System;

namespace Oxmes.Core.ServiceTypes
{
    /// <summary>
    /// This enum contain a mapping between known services and an integer serialisation of them.
    /// Due to forwards compatibility, this should only be used to guide, but services should
    /// primarily be kept as integers internally.
    /// </summary>
    public enum ServiceTypes
    {
        Topology = 1,
        Workplan = 2,
        Order = 3,
        OperationScheduling = 4,
        OrderScheduling = 5,
        Watchdog = 6,
        UserManagement = 7,
        TopologyDiscovery = 8,
        Analytics = 9,
        UserGateway = 100,
        PlcGateway = 150,
        UserClient = 200,
    }
}
